package com.lazymc.recoredutils;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dengzhijiang on 2016/1/6.
 */
public class AppBean {
    private int id;
    private String name;
    private String pkg;
    private Drawable icon;
    private String iconPath;
    private List<Notifycation> notifycation=new ArrayList<>(10);

    public void addNotify(Notifycation notifycation){
        this.notifycation.add(notifycation);
    }

    public void removeNotify(Notifycation notifycation){
        this.notifycation.remove(notifycation);
    }

    public final void notifycation(){
        for (int i = 0; i < notifycation.size(); i++) {
            notifycation.get(i).dataChange();
        }
    }

    public String getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(String totalSize) {
        this.totalSize = totalSize;
    }

    private String totalSize;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPkg() {
        return pkg;
    }

    public void setPkg(String pkg) {
        this.pkg = pkg;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public interface Notifycation{
        public void dataChange();
    }
}
