package com.lazymc.recoredutils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by dengzhijiang on 2016/1/6.
 */
public class Service {
    public final static ExecutorService poolService= Executors.newCachedThreadPool();

    public static void exe(Runnable runnable){
        poolService.execute(runnable);
    }

    public static void exe(Thread thread){
        poolService.execute(thread);
    }

    public static Future<?> subimt(Runnable runnable){
        return poolService.submit(runnable);
    }

    public static Future<?> subimt(Thread thread){
        return poolService.submit(thread);
    }
}
