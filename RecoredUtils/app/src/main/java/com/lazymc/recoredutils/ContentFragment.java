package com.lazymc.recoredutils;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.lazymc.recoredutils.provider.BusProvider;
import com.lazymc.recoredutils.provider.Reciver;
import com.lazymc.recoredutils.provider.UIThread;
import com.lazymc.recoredutils.utils.FindAppsUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dengzhijiang on 2016/1/6.
 */
public class  ContentFragment extends Fragment implements AppBean.Notifycation,Reciver{

    private List<AppBean> beanList = new ArrayList<AppBean>();
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private MAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        refreshLayout = new SwipeRefreshLayout(getContext());
        refreshLayout.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        recyclerView = new RecyclerView(getContext());
        recyclerView.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        refreshLayout.addView(recyclerView);
        mAdapter = new MAdapter();
        refreshLayout.setOnRefreshListener(() -> FindAppsUtils.findApps(getContext()));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        refreshLayout.setRefreshing(true);
        recyclerView.setAdapter(mAdapter);
        return refreshLayout;
    }

    @Override
    public void onStart() {
        super.onStart();
        FindAppsUtils.findApps(getContext());
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unRegister(this);
    }

    public UIThread callBack(List<AppBean> beanList) {
        ContentFragment.this.beanList.clear();
        if (beanList != null)
            ContentFragment.this.beanList.addAll(beanList);
        mAdapter.notifyDataSetChanged();
        refreshLayout.setRefreshing(false);
        return null;
    }

    public UIThread callBack() {
        mAdapter.notifyDataSetChanged();
        refreshLayout.setRefreshing(false);
        return null;
    }

    @Override
    public void dataChange() {
        mAdapter.notifyDataSetChanged();
    }

    public class DataHolder extends RecyclerView.ViewHolder{

        public DataHolder(View itemView) {
            super(itemView);
        }

        TextView tvName;
        TextView tvPkg;
        TextView tvPkgSize;
        SimpleDraweeView imageView;
        Button buttonGet;

        View view;

        public DataHolder(ViewPager viewPager) {
            super(viewPager);

        }

        void init(){
            tvName= (TextView) view.findViewById(R.id.tv_name);
            tvPkg=(TextView) view.findViewById(R.id.tv_name);
            tvPkgSize=(TextView) view.findViewById(R.id.tv_name);
            imageView= (SimpleDraweeView) view.findViewById(R.id.sdv_icon);
            buttonGet= (Button) view.findViewById(R.id.bt_get_app);
        }
    }

    public class PageAdapter extends PagerAdapter{
        public List<View> data=new ArrayList<>(2);
        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            container.removeView(data.get(position));
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
                return view==object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(data.get(position));
            return data.get(position);
        }
    }

    public class MAdapter extends RecyclerView.Adapter<DataHolder> {

        public Object getItem(int position) {
            return beanList.get(position);
        }

        @Override
        public DataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            ViewPager viewPager=new ViewPager(getContext());
            View view=LinearLayout.inflate(getContext(),R.layout.content_app_item,null);
            PageAdapter adapter=new PageAdapter();
            adapter.data.add(view);
            Button del=new Button(getContext());
            del.setLayoutParams(new ViewGroup.LayoutParams(200, -2));
            del.setOnClickListener((v) -> {
                Toast.makeText(getContext(), "del", Toast.LENGTH_SHORT).show();
            });
            del.setText("删除");
            del.setBackgroundColor(Color.RED);
            adapter.data.add(del);
            viewPager.setAdapter(adapter);
            viewPager.setLayoutParams(new ViewGroup.LayoutParams(-2, -1));
            DataHolder dataHolder= new DataHolder(view);
            dataHolder.view=view;
            dataHolder.init();
            return dataHolder;
        }

        @Override
        public void onBindViewHolder(DataHolder holder, int position) {
            AppBean bean= (AppBean) getItem(position);

            if (!TextUtils.isEmpty(bean.getName())){
                holder.tvName.setText(bean.getName());
            }
            if (!TextUtils.isEmpty(bean.getPkg())){
                holder.tvPkg.setText(bean.getPkg());
            }
            if (!TextUtils.isEmpty(bean.getTotalSize())){
                holder.tvPkgSize.setText(bean.getTotalSize());
            }
            if (bean.getIcon()!=null){
                holder.imageView.setImageDrawable(bean.getIcon());
            }else if(!TextUtils.isEmpty(bean.getIconPath())){
                holder.imageView.setImageURI(Uri.parse(bean.getIconPath()));
            }
            holder.buttonGet.setOnClickListener((v) -> {
                Toast.makeText(getContext(), "install", Toast.LENGTH_SHORT).show();
            });

        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public int getItemCount() {
            return beanList.size();
        }

    }

}
