package com.lazymc.recoredutils.utils;

import android.view.View;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dengzhijiang on 2016/1/7.
 */
public class ViewHolder {
    public static<T extends View>  T findView(View root,int rid){
        if (root.getTag()==null||!(root.getTag() instanceof Map)){
            Map<Integer,View> cache=new HashMap<>(4);
            View view=root.findViewById(rid);
            cache.put(rid,view);
            root.setTag(cache);
            return (T) view;
        }else{
            Map<Integer,View> cache= (Map<Integer, View>) root.getTag();
            if (cache.containsKey(rid)){
                return (T) cache.get(rid);
            }else{
                View view=root.findViewById(rid);
                cache.put(rid,view);
                return (T) view;
            }
        }
    }
}
